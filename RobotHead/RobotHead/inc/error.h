/*
 * error.h
 *
 *  Created on: Jun 30, 2014
 *      Author: raraujo
 */

#ifndef ERROR_H_
#define ERROR_H_
#include "chip.h"

#define TIMEOUT_ERROR		_BIT(0)
#define INSTRUCTION_ERROR	_BIT(1)


void printErrorTable(void);
void printError(uint8_t error);

#endif /* ERROR_H_ */
