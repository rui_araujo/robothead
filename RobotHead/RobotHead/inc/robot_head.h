#ifndef ROBOT_HEAD_H_
#define ROBOT_HEAD_H_

#include <stdint.h>

#define HEADER_VALUE		0x80

#define MAX_SPEED_MASK		0xFF

#define TOTAL_SERVOS		9

#define PACKET_SIZE			7

typedef enum statusCode {
	OK, INVALID_ID, TIMEOUT, STATUS_ERROR
} status_t;

#define SERVO_DEFAULT			1500

#define RETURN_OK				0x03

#define ID_ADDRESS				0x29
#define EEPROM_CHECKSUM_ADDRESS	0x2C

#define READ_EEPROM_COMMAND		0xE1
#define WRITE_EEPROM_COMMAND	0xE2
#define READ_MEMORY_COMMAND		0xE3
#define WRITE_MEMORY_COMMAND	0xE4
#define READ_POSITION_COMMAND	0xE5
#define READ_ID_COMMAND			0xE7
#define READ_VOLTAGE_COMMAND	0xE8
#define SET_SPEED_COMMAND		0xE9
#define SET_GAIN_COMMAND		0xEA
#define SET_MOTOR_COMMAND		0xEB
#define RELEASE_COMMAND			0xEF

void RobotHeadInit();
status_t writeToMemory(uint8_t address, uint8_t data);
status_t writeToEEPROM(uint8_t address, uint8_t data);
status_t readFromMemory(uint8_t address, uint8_t *data);
status_t readFromEEPROM(uint8_t address, uint8_t *data);
status_t getCorrectEEPROMCheckSum(uint8_t *data);
status_t updateEEPROMCheckSum();
status_t readVersionAndID(uint8_t *version, uint8_t*id);
status_t setPositionArm(uint8_t id, uint16_t goal);
uint32_t readPosition(uint8_t id);
uint16_t getPosition(uint8_t id);
status_t setSpeed(uint8_t id, uint8_t speed);
status_t readSpeed(uint8_t id);
uint16_t getSpeed(uint8_t id);
status_t setControlParameterSet(uint8_t set);
status_t setMotor(uint8_t go);
status_t readVoltage();
uint16_t getVoltage();
uint8_t getError(uint8_t id);
uint8_t getGlobalError();
status_t releaseServos();
#endif /* ROBOT_HEAD_H_ */
