/*
 * sensors.h
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#ifndef SENSORS_H_
#define SENSORS_H_
#include <stdint.h>

enum SensorIDs {
	POSITION0 = 0,//in uart.c it is assumed that the position sensors start at 0.
	POSITION1,
	POSITION2,
	POSITION3,
	POSITION4,
	POSITION5,
	POSITION6,
	POSITION7,
	POSITION8,
	ALL_POSITIONS,
	VOLTAGE
};

/**
 * Prints the sensor bitfield.
 */
void printSensorOptions(void);

/**
 * Initializes the sensor array with pointers for their functions
 */
extern void sensorsInit(void);

/**
 * It enables or disables a number of sensors based on the @flag and the @mask used.
 * @param mask bitfield where each bit corresponds to a different sensor
 * @param flag ENABLE or DISABLE
 * @param period the period used for the print out
 */
extern void enableSensors(uint32_t mask, uint8_t flag, uint32_t period);

/**
 * Invokes the refresh function of each sensor manually.
 * @param mask bitfield where each bit corresponds to a different sensor
 */
extern void getSensorsOutput(uint32_t mask);

#endif /* SENSORS_H_ */
