/*
 * error.c
 *
 *  Created on: Jun 30, 2014
 *      Author: raraujo
 */
#include "error.h"
#include "xprintf.h"
const char *errorVerbose[] = { "Timeout Error",  "Instruction Error"};

void printErrorTable(void) {
	xputs("Bitlist for the error code:\n");
	xputs(" Bit Dec-Value Description\n");
	xputs(" 0   1         Servo did not reply to command\n\n");
	xputs(" 1   2         Incorrect response\n\n");
}

void printError(uint8_t error) {
	for (int i = 0; i < (sizeof(errorVerbose) / sizeof(char*)); ++i) {
		if (error & _BIT(i)) {
			xprintf("%s\n", errorVerbose[i]);
		}
	}
}
