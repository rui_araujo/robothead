/*
 . * sensors.c
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#include "timers.h"
#include "xprintf.h"
#include <stdbool.h>
#include "chip.h"

void SysTick_Handler(void) {
	if (softTimers.timerDelay) {
		--softTimers.timerDelay;
	}
	for (int i = 0; i < softTimers.enabledTimersCounter; ++i) {
		if (--softTimers.enabledTimers[i]->counter == 0) {
			softTimers.enabledTimers[i]->counter = softTimers.enabledTimers[i]->reload;
			softTimers.enabledTimers[i]->triggered = 1;
			softTimers.refreshRequested = 1;
		}
	}
}

void timersInit(void) {
	softTimers.refreshRequested = 0;
	softTimers.timerDelay = 0;
	softTimers.enabledTimersCounter = 0;
	for (int i = 0; i < MAX_TIMERS; ++i) {
		softTimers.enabledTimers[i] = NULL;
		softTimers.timers[i].triggered = false;
		softTimers.timers[i].reload = 0;
		softTimers.timers[i].counter = 0;
		softTimers.timers[i].position = -1;
		softTimers.timers[i].arg = NULL;
		softTimers.timers[i].cb = NULL;
	}
	uint32_t load = Chip_Clock_GetSystemClockRate() / 1000 - 1;
	if (load > 0xFFFFFF) {
		load = 0xFFFFFF;
	}
	SysTick->LOAD = load;
	SysTick->CTRL |= 0x7;	//enable the Systick
}
void delayMs(uint32_t delay) {
	softTimers.timerDelay = delay;
	while (softTimers.timerDelay) {
		__asm volatile("nop");
	}
}

void updateTimer(int32_t timer, uint32_t period, void (*cb)(void *), void * arg) {
	if (cb == NULL) {
		return;
	}
	if (softTimers.timers[timer].position == -1) {
		return;
	}
	SysTick->CTRL &= ~0x1;	//disable the Systick
	softTimers.timers[timer].reload = period;	//Update the period
	softTimers.timers[timer].arg = arg;
	softTimers.timers[timer].cb = cb;
	SysTick->CTRL |= 0x1;	//enable the Systick
}

int16_t addTimer(uint32_t period, void (*cb)(void *), void * arg) {
	if (softTimers.enabledTimersCounter >= MAX_TIMERS) {
		return -2;
	}
	if (cb == NULL) {
		return -1;
	}
	SysTick->CTRL &= ~0x1;	//disable the Systick
	for (int i = 0; i < MAX_TIMERS; ++i) {
		if (softTimers.timers[i].position == -1) {
			softTimers.timers[i].counter = period;
			softTimers.timers[i].reload = period;
			softTimers.timers[i].position = softTimers.enabledTimersCounter;
			softTimers.enabledTimers[softTimers.timers[i].position] = &softTimers.timers[i];
			softTimers.enabledTimersCounter++;
			softTimers.timers[i].arg = arg;
			softTimers.timers[i].cb = cb;
			SysTick->CTRL |= 0x1;	//enable the Systick
			return i;
		}
	}
	SysTick->CTRL |= 0x1;	//enable the Systick
	return -2; // Shouldn't get here
}
void removeTimer(int32_t timerId) {
	if (timerId >= MAX_TIMERS || timerId < 0) {
		return;
	}
	if (softTimers.timers[timerId].position != -1) {
		SysTick->CTRL &= ~0x1;	//disable the Systick
		if (softTimers.timers[timerId].position == softTimers.enabledTimersCounter - 1) {
			softTimers.enabledTimers[softTimers.timers[timerId].position] = NULL;
		} else {
			//Move last to the current position
			int currentPosition = softTimers.timers[timerId].position;
			softTimers.enabledTimers[currentPosition] = softTimers.enabledTimers[softTimers.enabledTimersCounter - 1];
			softTimers.enabledTimers[currentPosition]->position = currentPosition;
			softTimers.enabledTimers[softTimers.enabledTimersCounter - 1] = NULL;
		}
		softTimers.timers[timerId].position = -1;
		softTimers.timers[timerId].triggered = 0;
		softTimers.enabledTimersCounter--;
		SysTick->CTRL |= 0x1;	//enable the Systick
	}
}

void runTimersCB(uint8_t mask) {
	for (int i = 0; i < MAX_TIMERS; ++i) {
		if (mask & (1 << i)) {
			if (softTimers.timers[i].cb != NULL) {
				softTimers.timers[i].cb(softTimers.timers[i].arg);
			}
		}
	}
}
