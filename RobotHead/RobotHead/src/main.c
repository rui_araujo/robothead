#include <cr_section_macros.h>
#include "chip.h"
#include "uart.h"
#include "sensors.h"
#include "robot_head.h"
#include "pinout.h"
#include "timers.h"

#define LED_PERIOD		(1000)

void toggleLed(void*arg) {
	Chip_GPIO_SetPinToggle(LPC_GPIO, LED_PORT, LED_PIN);
}

int main(void) {
	timersInit();
	RobotHeadInit();
	sensorsInit();
	UARTsInit();
	UARTShowVersion();
	Chip_GPIO_SetPinOutHigh(LPC_GPIO, LED_PORT, LED_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, LED_PORT, LED_PIN);
	addTimer(LED_PERIOD, toggleLed, NULL);
	while (1) {
		if (RingBuffer_GetCount(&uart0RX)) {
			UART0ParseNewChar();
			if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
				//Please stop!
				Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			} else {
				//Ready to do business!
				Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			}
		}
		processSoftTimers();
		if (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN) == 0) { // no rts stop signal
			if (!clearToSend && !RingBuffer_IsEmpty(&uart0TX)) {
				UARTRestartTX();
			}
		}
	}
	return 0;
}
