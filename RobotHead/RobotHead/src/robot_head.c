#include "chip.h"
#include "packet.h"
#include "robot_head.h"
#include "uart.h"
#include "error.h"
#include "utils.h"
#include <string.h>

#define TIMEOUT_VALUE 60000 //random big number#define DEFAULT_SPEED		(50)

enum positions {
	HEADER_POSITION = 0, COMMAND_POSITION, PARAM1_POSITION, PARAM2_POSITION, CHECKSUM_POSITION, RETURN1_POSITION, RETURN2_POSITION
};

struct dynamixelServo {
	uint8_t id;
	uint8_t currentSpeed;
	uint8_t error; /*Bit field */
	uint16_t desiredPosition;
	uint16_t currentPosition;
};

struct {
	uint16_t packetBufferTx[PACKET_SIZE];
	uint16_t packetBufferRx[PACKET_SIZE];
	struct dynamixelServo servos[TOTAL_SERVOS];
	uint8_t error; /*Bit field */
	uint8_t pulsewidth;
	uint32_t currentVoltage;
} robotHead;

static Chip_SSP_DATA_SETUP_T spibuffer;

void RobotHeadInit() {
	memset(&robotHead, 0, sizeof(robotHead));
	for (int i = 0; i < TOTAL_SERVOS; ++i) {
		robotHead.servos[i].id = i;
		robotHead.servos[i].currentSpeed = DEFAULT_SPEED;
	}
	memset(&spibuffer, 0, sizeof(spibuffer));
	spibuffer.rx_data = robotHead.packetBufferRx;
	spibuffer.tx_data = robotHead.packetBufferTx;
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_SSP1);
	Chip_SSP_Set_Mode(LPC_SSP1, SSP_MODE_MASTER);
	Chip_SSP_SetFormat(LPC_SSP1, SSP_BITS_11, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_CPHA0_CPOL0);
	Chip_SSP_SetBitRate(LPC_SSP1, 19200);
	Chip_SSP_Enable(LPC_SSP1);
}

static uint8_t calculateCheckSum(uint16_t *data) {
	uint32_t checksum = 0;
	for (int i = 0; i < CHECKSUM_POSITION; ++i) {
		checksum += data[i];
	}
	return 256 - (checksum & 0xFF);
}

static uint32_t invertAndSwap8Bits(uint32_t arg) {
	uint32_t result = ~(__RBIT(arg));
	return (result >> 24) & 0xFF;
}

static uint16_t convertToInvertedRS232(uint16_t arg) {
	uint16_t result = (uint16_t) invertAndSwap8Bits(arg & 0xFF);
	result <<= 2;
	result |= (uint16_t) 0x400;
	return result;
}
static uint16_t convertFromInvertedRS232(uint16_t arg) {
	uint16_t result = arg >> 2;
	result = (uint16_t) invertAndSwap8Bits(result & 0xFF);
	return result;
}

static uint32_t sendPacket() {
//Checksum calculation
	robotHead.packetBufferTx[HEADER_POSITION] = HEADER_VALUE;
	robotHead.packetBufferTx[CHECKSUM_POSITION] = calculateCheckSum(robotHead.packetBufferTx);
	robotHead.packetBufferTx[RETURN1_POSITION] = 0;
	robotHead.packetBufferTx[RETURN2_POSITION] = 0;
	memset(robotHead.packetBufferRx, 0, sizeof(robotHead.packetBufferRx));
	for (int i = 0; i < PACKET_SIZE; ++i) {
		robotHead.packetBufferTx[i] = convertToInvertedRS232(robotHead.packetBufferTx[i]);
		LPC_SSP1->DR = robotHead.packetBufferTx[i];
	}
	for (int i = 0; i < PACKET_SIZE; ++i) {
		uint32_t timeout = TIMEOUT_VALUE;
		while (!Chip_SSP_GetStatus(LPC_SSP1, SSP_STAT_RNE)) {
			if (timeout-- == 0)
				return TIMEOUT;
		}
		robotHead.packetBufferRx[i] = (uint16_t) LPC_SSP1->DR;
	}
	return OK;
}

status_t writeToMemory(uint8_t address, uint8_t data) {
	robotHead.packetBufferTx[COMMAND_POSITION] = WRITE_MEMORY_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = address;
	robotHead.packetBufferTx[PARAM2_POSITION] = data;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN1_POSITION] == RETURN_OK && robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}
status_t writeToEEPROM(uint8_t address, uint8_t data) {
	if (address > EEPROM_CHECKSUM_ADDRESS) {
		robotHead.error = INSTRUCTION_ERROR;
		return STATUS_ERROR;
	}
	robotHead.packetBufferTx[COMMAND_POSITION] = WRITE_EEPROM_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = address;
	robotHead.packetBufferTx[PARAM2_POSITION] = data;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	timerDelayMs(50); // to prevent problems after writing
	if (robotHead.packetBufferRx[RETURN1_POSITION] == RETURN_OK && robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}

status_t getCorrectEEPROMCheckSum(uint8_t *checksumOut) {
	if (!checksumOut) {
		return STATUS_ERROR;
	}
	status_t error;
	uint8_t data = 0;
	uint32_t checksum = 0;
	for (uint8_t address = 0; address < EEPROM_CHECKSUM_ADDRESS; ++address) {
		error = readFromEEPROM(address, &data);
		if (error) {
			robotHead.error = INSTRUCTION_ERROR;
			return error;
		}
		checksum += data;
	}
	*checksumOut = 256 - (checksum & 0xFF);
	return OK;
}
status_t updateEEPROMCheckSum() {
	uint8_t checksum;
	status_t error = getCorrectEEPROMCheckSum(&checksum);
	if (error) {
		return error;
	}
	return writeToEEPROM(EEPROM_CHECKSUM_ADDRESS, checksum);
}
status_t readFromMemory(uint8_t address, uint8_t *data) {
	if (!data) {
		return STATUS_ERROR;
	}
	robotHead.packetBufferTx[COMMAND_POSITION] = READ_MEMORY_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = address;
	robotHead.packetBufferTx[PARAM2_POSITION] = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		*data = robotHead.packetBufferRx[RETURN1_POSITION];
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}
status_t readFromEEPROM(uint8_t address, uint8_t *data) {
	if (!data) {
		return STATUS_ERROR;
	}
	robotHead.packetBufferTx[COMMAND_POSITION] = READ_EEPROM_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = address;
	robotHead.packetBufferTx[PARAM2_POSITION] = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		*data = robotHead.packetBufferRx[RETURN1_POSITION];
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}

status_t readVersionAndID(uint8_t *version, uint8_t*id) {
	if (!version || !id) {
		return STATUS_ERROR;
	}
	robotHead.packetBufferTx[COMMAND_POSITION] = READ_ID_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = 0;
	robotHead.packetBufferTx[PARAM2_POSITION] = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	*version = robotHead.packetBufferRx[RETURN1_POSITION];
	*id = robotHead.packetBufferRx[RETURN2_POSITION];
	return OK;
}

status_t setPositionArm(uint8_t id, uint16_t goal) {
	if (id > TOTAL_SERVOS) {
		return INVALID_ID;
	}
	robotHead.servos[id].desiredPosition = goal;
	robotHead.packetBufferTx[COMMAND_POSITION] = id;
	robotHead.packetBufferTx[PARAM1_POSITION] = goal >> 8;
	robotHead.packetBufferTx[PARAM2_POSITION] = goal & 0xFF;
	robotHead.servos[id].error = 0;
	if (sendPacket()) {
		robotHead.servos[id].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	return OK;
}

uint32_t readPosition(uint8_t id) {
	if (id > TOTAL_SERVOS) {
		return INVALID_ID;
	}
	return setSpeed(id, robotHead.servos[id].currentSpeed);
}

uint16_t getPosition(uint8_t id) {
	return robotHead.servos[id].currentPosition;
}

status_t setSpeed(uint8_t id, uint8_t speed) {
	if (id > TOTAL_SERVOS) {
		return INVALID_ID;
	}
	if (speed == 0) {
		speed = robotHead.servos[id].currentSpeed;
	} else {
		speed &= MAX_SPEED_MASK;
		robotHead.servos[id].currentSpeed = speed;
	}

	robotHead.packetBufferTx[COMMAND_POSITION] = SET_SPEED_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = id;
	robotHead.packetBufferTx[PARAM2_POSITION] = speed;
	robotHead.servos[id].error = 0;
	if (sendPacket()) {
		robotHead.servos[id].error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	//We get the position as a reply of this command
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	robotHead.servos[id].currentPosition = (robotHead.packetBufferRx[RETURN1_POSITION] << 8) | robotHead.packetBufferRx[RETURN2_POSITION];
	return OK;
}

uint16_t getSpeed(uint8_t id) {
	return robotHead.servos[id].currentSpeed;
}

status_t setControlParameterSet(uint8_t set) {
	robotHead.packetBufferTx[COMMAND_POSITION] = SET_GAIN_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = 0;
	robotHead.packetBufferTx[PARAM2_POSITION] = set;
	robotHead.error = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN1_POSITION] == RETURN_OK && robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}

status_t setMotor(uint8_t go) {
	robotHead.packetBufferTx[COMMAND_POSITION] = SET_MOTOR_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = 0;
	robotHead.packetBufferTx[PARAM2_POSITION] = go ? 1 : 0;
	robotHead.error = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN1_POSITION] == RETURN_OK && robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}

#define VOLTAGE_SCALE	3522
status_t readVoltage() {
	robotHead.packetBufferTx[COMMAND_POSITION] = READ_VOLTAGE_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = 0;
	robotHead.packetBufferTx[PARAM2_POSITION] = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	robotHead.pulsewidth = robotHead.packetBufferRx[RETURN1_POSITION];
	robotHead.currentVoltage = (robotHead.packetBufferRx[RETURN2_POSITION] * VOLTAGE_SCALE) / 100;
	return OK;
}

uint16_t getVoltage() {
	return robotHead.currentVoltage;
}
uint8_t getError(uint8_t id) {
	return robotHead.servos[id].error;
}

uint8_t getGlobalError() {
	return robotHead.error;
}
status_t releaseServos() {
	robotHead.packetBufferTx[COMMAND_POSITION] = RELEASE_COMMAND;
	robotHead.packetBufferTx[PARAM1_POSITION] = 0;
	robotHead.packetBufferTx[PARAM2_POSITION] = 0;
	if (sendPacket()) {
		robotHead.error = TIMEOUT_ERROR;
		return TIMEOUT;
	}
	robotHead.packetBufferRx[RETURN1_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN1_POSITION]);
	robotHead.packetBufferRx[RETURN2_POSITION] = convertFromInvertedRS232(robotHead.packetBufferRx[RETURN2_POSITION]);
	if (robotHead.packetBufferRx[RETURN1_POSITION] == RETURN_OK && robotHead.packetBufferRx[RETURN2_POSITION] == RETURN_OK) {
		return OK;
	}
	robotHead.error = INSTRUCTION_ERROR;
	return STATUS_ERROR;
}
