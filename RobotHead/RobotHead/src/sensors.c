/*
 * sensors.c
 *
 *  Created on: Apr 10, 2014
 *      Author: raraujo
 */

#include "chip.h"
#include "sensors.h"
#include "xprintf.h"
#include "robot_head.h"
#include "timers.h"
#include <stdbool.h>

#define TOTAL_SENSORS			(11)

struct sensor {
	int32_t timerId; /* Position in the enabled sensors queue */
	void (*cb)(void *); /* pointer to the refresh function where the values are printed*/
	void * arg; /*pointer to possible argument*/
};

static struct sensor sensors[TOTAL_SENSORS];

void printSensorOptions(void) {
	xputs("Bitlist for available sensors:\n");
	xputs(" Bit Dec-Value Name      # Values  Description\n");
	xputs(" 0   1         POSITION0        1  servo0 position\n");
	xputs(" 1   2         POSITION1        1  servo1 position\n");
	xputs(" 2   4         POSITION2        1  servo2 position\n");
	xputs(" 3   8         POSITION3        1  servo3 position\n");
	xputs(" 4   16        POSITION4        1  servo4 position\n");
	xputs(" 5   32        POSITION5        1  servo5 position\n");
	xputs(" 6   64        POSITION6        1  servo6 position\n");
	xputs(" 7   128       POSITION7        1  servo7 position\n");
	xputs(" 8   256       POSITION8        1  servo8 position\n");
	xputs(" 9   512       ALL POSITIONS    9  all servos positions\n");
	xputs(" 10  1024      VOLTAGE          1  servos' voltage\n");
}

void allPositionsReport(void *arg) {
	xputs("-PA");
	for (int i = 0; i < TOTAL_SERVOS; ++i) {
		if (readPosition(i)) {
			xprintf(" -%d", getError(i));
		} else {
			xprintf(" %d", getPosition(i));
		}
	}
	xputc('\n');
}
void positionsReport(void *arg) {
	int id = (int) arg; //This is evil but easy
	if (readPosition(id)) {
		xprintf("-P%d -%d\n", id, getError(id));
	} else {
		xprintf("-P%d %d\n", id, getPosition(id));
	}

}

void voltageReport(void *arg) {
	if (readVoltage()) {
		xprintf("-V-%d\n", getGlobalError());
	} else {
		xprintf("-V%d\n", getVoltage());
	}
}

void sensorsInit(void) {
	for (int i = 0; i < TOTAL_SENSORS; ++i) {
		sensors[i].timerId = -1;
		switch (i) {
		case 9:
			sensors[i].cb = allPositionsReport;
			sensors[i].arg = NULL;
			break;
		case 10:
			sensors[i].cb = voltageReport;
			sensors[i].arg = NULL;
			break;
		default:
			sensors[i].cb = positionsReport;
			sensors[i].arg = (void *) i; //This is evil but easy
			break;
		}
	}
}

void enableSensor(uint32_t sensorId, uint8_t flag, uint32_t period) {
	if (sensorId >= TOTAL_SENSORS) {
		return;
	}
	if (sensors[sensorId].cb == NULL) {
		return;
	}
	if (flag) {
		if (sensors[sensorId].timerId == -1) {
			sensors[sensorId].timerId = addTimer(period, sensors[sensorId].cb, sensors[sensorId].arg);
		} else {
			updateTimer(sensors[sensorId].timerId, period, sensors[sensorId].cb, sensors[sensorId].arg);
		}
	} else {
		if (sensors[sensorId].timerId != -1) {
			removeTimer(sensors[sensorId].timerId);
			sensors[sensorId].timerId = -1;
		}
	}
}

void enableSensors(uint32_t mask, uint8_t flag, uint32_t period) {
	for (int i = 0; i < TOTAL_SENSORS; ++i) {
		if (mask & (1 << i)) {
			enableSensor(i, flag, period);
		}
	}
}

void getSensorsOutput(uint32_t mask) {
	for (int i = 0; i < TOTAL_SENSORS; ++i) {
		if (mask & (1 << i)) {
			sensors[i].cb(sensors[i].arg);
		}
	}
}

