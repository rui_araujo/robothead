#include <string.h>
#include "chip.h"
#include "robot_head.h"
#include "utils.h"
#include "uart.h"
#include "xprintf.h"
#include "error.h"
#include "sensors.h"
#include "pinout.h"
#include <stdbool.h>
#include <ctype.h>

#define SOFTWARE_VERSION "1.0"

#define DEFAULT_BAUDRATE_UART0			(1000000)

#define TX_BUFFER_SIZE_BITS		(12)
#define TX_BUFFER_SIZE			(1<<TX_BUFFER_SIZE_BITS)

#define RX_BUFFER_SIZE_BITS		(12)
#define RX_BUFFER_SIZE			(1<<RX_BUFFER_SIZE_BITS)

//Transmit buffer that will be used on the rest of the system
RINGBUFF_T uart0TX;
RINGBUFF_T uart0RX;

volatile bool clearToSend = false;

char uart0TxBuffer[TX_BUFFER_SIZE];
char uart0RxBuffer[RX_BUFFER_SIZE];

#define UART_COMMAND_LINE_MAX_LENGTH  	128

// *****************************************************************************

unsigned char commandLine[UART_COMMAND_LINE_MAX_LENGTH];
uint32_t commandLinePointer;
uint32_t enableUARTecho;	 // 0-no cmd echo, 1-all visible

// *****************************************************************************
#define UARTReturn()	   xputc('\n')

/* UART transmit-only interrupt handler for ring buffers */
static inline void Chip_UART_TXIntHandlerFlowControl() {
	uint8_t ch;

	/* Fill FIFO until full or until TX ring buffer is empty */
	if (Chip_GPIO_ReadPortBit(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN) == 0) { // no rts stop signal
		clearToSend = true;
		while ((Chip_UART_ReadLineStatus(LPC_UART0) & UART_LSR_THRE) != 0 && RingBuffer_Pop(&uart0TX, &ch)) {
			Chip_UART_SendByte(LPC_UART0, ch);
		}
	} else {
		clearToSend = false;
	}
}

void UARTRestartTX() {
	/* Don't let UART transmit ring buffer change in the UART IRQ handler */
	Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);
	Chip_UART_TXIntHandlerFlowControl();
	/* Enable UART transmit interrupt */
	Chip_UART_IntEnable(LPC_UART0, UART_IER_THREINT);
}

void UART0WriteChar(char pcBuffer) {

	uint32_t ret;
	do {
		/* Don't let UART transmit ring buffer change in the UART IRQ handler */
		Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);

		/* Move as much data as possible into transmit ring buffer */
		ret = RingBuffer_Insert(&uart0TX, &pcBuffer);
		Chip_UART_TXIntHandlerFlowControl();

		/* Enable UART transmit interrupt */
		Chip_UART_IntEnable(LPC_UART0, UART_IER_THREINT);
	} while (ret == 0);
}

void UART0_IRQHandler(void) {

	uint32_t interruptIdentifier;
	/* Handle transmit interrupt if enabled */
	if (LPC_UART0->IER & UART_IER_THREINT) {
		Chip_UART_TXIntHandlerFlowControl();

		/* Disable transmit interrupt if the ring buffer is empty */
		if (RingBuffer_IsEmpty(&uart0TX)) {
			Chip_UART_IntDisable(LPC_UART0, UART_IER_THREINT);
		}
	}
	while (((interruptIdentifier = LPC_UART0->IIR) & 0x01) == 0) {
		if (interruptIdentifier & UART_IIR_INTID_RDA) {
			/* Handle receive interrupt */
			Chip_UART_RXIntHandlerRB(LPC_UART0, &uart0RX);
			if (RingBuffer_GetFree(&uart0RX) < CTS_BUFFER_THRESHOLD) {
				//Please stop!
				Chip_GPIO_SetPinOutHigh(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			} else {
				//Ready to do business!
				Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
			}
		}
	}
}

void UARTsInit() {
	memset(commandLine, 0, UART_COMMAND_LINE_MAX_LENGTH);
	commandLinePointer = 0;
	enableUARTecho = 1;
	RingBuffer_Init(&uart0TX, uart0TxBuffer, 1, TX_BUFFER_SIZE);
	RingBuffer_Init(&uart0RX, uart0RxBuffer, 1, RX_BUFFER_SIZE);
	Chip_GPIO_SetPinOutLow(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO, UART0_RTS_PORT, UART0_RTS_PIN);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, UART0_CTS_PORT, UART0_CTS_PIN);
	xdev_out(UART0WriteChar);
	Chip_UART_Init(LPC_UART0);
	Chip_UART_ConfigData(LPC_UART0, (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT));
	Chip_UART_SetupFIFOS(LPC_UART0, (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2));
	Chip_UART_IntEnable(LPC_UART0, UART_IER_RBRINT);
	Chip_UART_SetBaudFDR(LPC_UART0, DEFAULT_BAUDRATE_UART0);
	Chip_UART_TXEnable(LPC_UART0);
	NVIC_EnableIRQ(UART0_IRQn);
}

// *****************************************************************************

// *****************************************************************************
void UARTShowVersion(void) {
	xputs("\nHitecDriverLPC1769, V" SOFTWARE_VERSION " " __DATE__ ", " __TIME__ "\n");
}

// *****************************************************************************
static void UARTShowUsage(void) {

	UARTShowVersion();

	UARTReturn();
	xputs("Supported Commands:\n");
	UARTReturn();

	xputs(" ?I                  - get servo id and version\n");
	xputs(" !I=b                - set servo id to b\n");
	UARTReturn();

	xputs(" ?C                  - display checksums(the current and correct)\n");
	UARTReturn();

	xputs(" !D+b,p              - enable sensors streaming, ??D to show options\n");
	xputs(" !D-[b]              - disable sensors streaming, ??D to show options\n");
	xputs(" ?Db                 - get sensor readouts according to bitfield b\n");
	xputs(" ??D                 - bitfield b options\n");
	UARTReturn();

	xputs(" ?Ex                 - print verbose explanation of the error x\n");
	xputs(" ??E                 - print error table\n");
	UARTReturn();

	xputs(" ?Maddress           - read address from servo memory\n");
	xputs(" !Maddress=data      - write to servo memory address\n");
	xputs(" ?MEaddress          - read address from servo EEPROM memory\n");
	xputs(" !MEaddress=data     - write to servo EEPROM memory address\n");
	UARTReturn();

	xputs(" !P[0-8],p           - set servo position\n");
	xputs(" !PA[0-8],p[|]       - set multiple servos position, | as separator of id,p\n");
	xputs(" !PR                 - reset all servos position to middle\n");
	xputs(" ?P[0-8]             - get servo position\n");
	xputs(" ?PA                 - get all servos positions\n");
	UARTReturn();

	xputs(" !R                  - release servos\n");
	UARTReturn();

	xputs(" !S[0-8],s           - set servo velocity\n");
	xputs(" !SA[0-8],s[|]       - set multiple servos velocity, | as separator of id,s\n");
	xputs(" ?S[0-8]             - get servo velocity\n");
	xputs(" ?SA                 - get all servos velocities\n");
	UARTReturn();

	xputs(" ?V                  - get servos voltage\n");
	UARTReturn();

	xputs(" !U=x                - set baud rate to x\n");
	xputs(" !U[0,1]             - UART echo mode (none, all)\n");
	UARTReturn();

	xputs(" ??                  - display (this) help\n");
	UARTReturn();
}

// *****************************************************************************
static uint32_t parseUInt32(unsigned char **c) {
	uint32_t ul = 0;
	while (((**c) >= '0') && ((**c) <= '9')) {
		ul = 10 * ul;
		ul += ((**c) - '0');
		(*(c))++;
	}
	return (ul);
}
static int32_t parseInt32(unsigned char **c) {
	if ((**c) == '-') {
		(*(c))++;
		return (-1 * ((int32_t) parseUInt32(c)));
	}
	if ((**c) == '+') {
		(*(c))++;
	}
	return ((int32_t) parseUInt32(c));
}

// *****************************************************************************
// * ** parseGetCommand ** */
// *****************************************************************************
static void UARTParseGetCommand(void) {

	switch (commandLine[1]) {

	case '?':
		if (commandLine[2] == 'e' || commandLine[2] == 'E') {
			printErrorTable();
			break;
		}
		if (((commandLine[2]) == 'd') || ((commandLine[2]) == 'D')) {
			printSensorOptions();
			break;
		}
		UARTShowUsage();
		break;
	case 'c':
	case 'C': {
		uint8_t correctChecksum, currentChecksum;
		if (getCorrectEEPROMCheckSum(&correctChecksum)) {
			xprintf("-C-%d\n", getGlobalError());
		}
		if (readFromEEPROM(EEPROM_CHECKSUM_ADDRESS, &currentChecksum)) {
			xprintf("-C-%d\n", getGlobalError());
			break;
		}
		xprintf("-C%d %d\n", currentChecksum, correctChecksum);
		break;
	}
	case 'd':
	case 'D': {
		unsigned char *c = commandLine + 2;
		if (!isdigit(*c)) {
			xputs("Bitfield must must be an integer\n");
			break;
		}
		getSensorsOutput(parseUInt32(&c));
		break;
	}
	case 'e':
	case 'E': {
		unsigned char *c = commandLine + 2;
		int error = parseInt32(&c);
		if (error < 0) {
			error = -error;
		}
		printError(error);
		break;
	}
	case 'i':
	case 'I': {
		uint8_t version, id;
		if (readVersionAndID(&version, &id)) {
			xprintf("-I-%d\n", getGlobalError());
		}
		xprintf("-I%d %d\n", id, version);
		break;
	}
	case 'm':
	case 'M': {
		unsigned char *c = commandLine + 2;
		bool eeprom = false;
		if (*c == 'E' || *c == 'e') {
			eeprom = true;
			c++;
		}
		if (!isdigit(*c)) {
			xputs("Address must must be an integer\n");
			break;
		}
		uint32_t address = parseUInt32(&c);
		if (address > 0xFF) {
			xputs("Address must be an integer between 0 and 255\n");
			break;
		}
		uint8_t data = 0;
		if (eeprom) {
			if (readFromEEPROM(address, &data)) {
				xprintf("-M%d -%d\n", address, getGlobalError());
				break;
			}

		} else {
			if (readFromMemory(address, &data)) {
				xprintf("-M%d -%d\n", address, getGlobalError());
				break;
			}
		}
		xprintf("-M%d %d\n", address, data);
		break;
	}
	case 'p':
	case 'P': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			getSensorsOutput(_BIT(ALL_POSITIONS));
			break;
		} else if (*c >= '0' && *c < ('0' + TOTAL_SERVOS)) {
			uint8_t currentId = *c - '0';
			getSensorsOutput(_BIT(currentId));
			break;
		}
		break;
	}
	case 's':
	case 'S': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			xputs("-SA");
			for (int i = 0; i < TOTAL_SERVOS; ++i) {
				xprintf(" %d", getSpeed(i));
			}
			xputc('\n');
			break;
		} else if (*c >= '0' && *c < ('0' + TOTAL_SERVOS)) {
			uint8_t currentId = *c - '0';
			xprintf("-S%d %d\n", currentId, getSpeed(currentId));
			break;
		}
		break;
	}
	case 'v':
	case 'V': {
		getSensorsOutput(_BIT(VOLTAGE));
		break;
	}
	default:
		xputs("Get: parsing error\n");
	}
}

// *****************************************************************************
// * ** parseSetCommand ** */
// *****************************************************************************
static void UARTParseSetCommand(void) {
	switch (commandLine[1]) {

	case 'D':
	case 'd': {
		unsigned char *c = commandLine + 2;
		uint8_t flag = 0;
		if (*c == '+') {
			flag = ENABLE;
		} else if (*c == '-') {
			flag = DISABLE;
		} else {
			break;
		}
		c++;
		if (!isdigit(*c)) {
			if (!flag && (commandLinePointer == 3)) {
				enableSensors(0xFFFFFFFF, flag, 0);
				return;
			}
			break;
		}
		uint32_t mask = parseUInt32(&c);
		if (*c == ',') {
			c++;
		} else {
			if (flag) {
				xputs("Set: parsing error\n");
				break; //second argument only mandatory when enabling.
			}
		}
		uint32_t period = 1;
		if (flag) {
			if (!isdigit(*c)) {
				xputs("Set: parsing error\n");
				break;
			}
			period = parseUInt32(&c);
		}
		enableSensors(mask, flag, period);
		return;
	}
	case 'i':
	case 'I': {
		unsigned char *c = commandLine + 2;
		if (*c != '=') {
			break;
		}
		c++;
		if (!isdigit(*c)) {
			xputs("Id must be an integer\n");
			return;
		}
		uint32_t id = parseUInt32(&c);
		if (id > 0xFF) {
			xputs("Id must be an integer between 0 and 255\n");
			return;
		}
		if (writeToEEPROM(ID_ADDRESS, id)) {
			xprintf("-I%d -%d\n", id, getGlobalError());
			return;
		}
		//An update to the checksum is needed otherwise the motors don't move
		if (updateEEPROMCheckSum()) {
			xprintf("-I%d -%d\n", id, getGlobalError());
			xputs("Checksum Update failed. Motor will not move.\n");
			break;
		}
		uint8_t version, newId;
		if (readVersionAndID(&version, &newId)) {
			xprintf("-I%d -%d\n", id, getGlobalError());
			return;
		}
		if (newId != id) {
			xputs("Id will only be updated after power cycle.\n");
		}
		xprintf("-I%d\n", newId);
		return;
	}
	case 'm':
	case 'M': {
		unsigned char *c = commandLine + 2;
		bool eeprom = false;
		if (*c == 'E' || *c == 'e') {
			eeprom = true;
			c++;
		}
		if (!isdigit(*c)) {
			xputs("Address must must be an integer\n");
			break;
		}
		uint32_t address = parseUInt32(&c);
		if (address > 0xFF) {
			xputs("Address must be an integer between 0 and 0xFF\n");
			break;
		}
		if (*c != '=') {
			xputs("Get: parsing error\n");
			break;
		}
		c++;
		uint32_t data = parseUInt32(&c);
		if (data > 0xFF) {
			xputs("Data must be an integer between 0 and 0xFF\n");
			break;
		}
		if (eeprom) {
			if (writeToEEPROM(address, data)) {
				xprintf("-M%d -%d\n", address, getGlobalError());
				break;
			}
			//An update to the checksum is needed otherwise the motors don't move
			if (updateEEPROMCheckSum()) {
				xprintf("-M%d -%d\n", address, getGlobalError());
				xputs("Checksum Update failed. Motor will not move.\n");
				break;
			}
		} else {
			if (writeToMemory(address, data)) {
				xprintf("-M%d -%d\n", address, getGlobalError());
				break;
			}
		}
		xprintf("-M%d %d\n", address, data);
		break;
	}
	case 'p':
	case 'P': {
		unsigned char *c = commandLine + 2;
		if (*c == 'r' || *c == 'R') {
			xputs("-PR\n");
			for (int i = 0; i < TOTAL_SERVOS; ++i) {
				setPositionArm(i, SERVO_DEFAULT);
			}
			break;
		} else if (*c == 'a' || *c == 'A') {
			c++;
			bool noErrors = true;
			while (noErrors) {
				if (*c < '0' || *c >= ('0' + TOTAL_SERVOS)) {
					xputs("Set: wrong id\n");
					noErrors = false;
					continue;
				}
				uint8_t currentId = *c - '0';
				c++;
				if (*c == ',') {
					c++;
				} else {
					xputs("Set: parsing error\n");
					noErrors = false;
					continue;
				}
				uint16_t goal = parseUInt32(&c);
				setPositionArm(currentId, goal);
				if (*c == '|') {
					c++;
					if (*c == '\0') { //We have reached the end
						break;
					}
				} else { //We have reached the end
					break;
				}
			}
			if (noErrors) {
				xputs("-PA\n");
			}
			break;
		} else if (*c >= '0' && *c < ('0' + TOTAL_SERVOS)) {
			uint8_t currentId = *c - '0';
			c++;
			if (*c == ',') {
				c++;
			} else {
				xputs("Set: parsing error\n");
				break;
			}
			uint16_t goal = parseUInt32(&c);
			if (setPositionArm(currentId, goal)) {
				xprintf("-P%d -%d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-P%d\n", currentId);
			break;
		}
	}
	case 'r':
	case 'R': {
		if (releaseServos()) {
			xprintf("-R-%d\n", getGlobalError());
			break;
		}
		xputs("-R\n");
		break;
	}
	case 's':
	case 'S': {
		unsigned char *c = commandLine + 2;
		if (*c == 'a' || *c == 'A') {
			c++;
			bool noErrors = true;
			while (noErrors) {
				if (*c < '0' || *c >= ('0' + TOTAL_SERVOS)) {
					xputs("Set: wrong id\n");
					noErrors = false;
					continue;
				}
				uint8_t currentId = *c - '0';
				c++;
				if (*c == ',') {
					c++;
				} else {
					xputs("Set: parsing error\n");
					noErrors = false;
					continue;
				}
				uint8_t speed = parseUInt32(&c);
				setSpeed(currentId, speed);
				if (*c == '|') {
					c++;
					if (*c == '\0') { //We have reached the end
						break;
					}
				} else { //We have reached the end
					break;
				}
			}
			if (noErrors) {
				xputs("-SA\n");
			}
			break;
		} else if (*c >= '0' && *c < ('0' + TOTAL_SERVOS)) {
			uint8_t currentId = *c - '0';
			c++;
			if (*c == ',') {
				c++;
			} else {
				xputs("Set: parsing error\n");
				break;
			}
			uint8_t speed = parseUInt32(&c);
			if (setSpeed(currentId, speed)) {
				xprintf("-S%d -%d\n", currentId, getError(currentId));
				break;
			}
			xprintf("-S%d %d\n", currentId, getPosition(currentId));
			break;
		}
	}
	case 'U':
	case 'u': {
		unsigned char *c;
		long baudRate;
		c = commandLine + 2;
		if (((*c) >= '0') && ((*c) <= '2')) {
			enableUARTecho = ((*c) - '0');
			break;
		}
		c++;
		baudRate = parseUInt32(&c);
		while ((LPC_UART0->LSR & UART_LSR_TEMT) == 0) {
		};		   // wait for UART to finish data transfer
		xprintf("-U=%d\n", baudRate);
		timerDelayMs(100);
		if (Chip_UART_SetBaudFDR(LPC_UART0, baudRate) == 0) {
			xprintf("Failed to switch Baud Rate to %d Baud!\n", baudRate);
		}
		break;
	}

	default:
		xputs("Set: parsing error\n");
	}
}

// *****************************************************************************
// * ** parseRS232CommandLine ** */
// *****************************************************************************
static void parseRS232CommandLine(void) {

	switch (commandLine[0]) {
	case '?':
		UARTParseGetCommand();
		break;
	case '!':
		UARTParseSetCommand();
		break;
	default:
		xputs("?\n");
	}
}

// *****************************************************************************
// * ** RS232ParseNewChar ** */
// *****************************************************************************
void UART0ParseNewChar() {
	unsigned char newChar;
	RingBuffer_Pop(&uart0RX, &newChar);
	switch (newChar) {
	case 8:			// backspace
		if (commandLinePointer > 0) {
			commandLinePointer--;
			if (enableUARTecho) {
				xprintf("%c %c", 8, 8);
			}
		}
		break;

	case 10:
	case 13:
		if (enableUARTecho) {
			UARTReturn();
		}
		if (commandLinePointer > 0) {
			commandLine[commandLinePointer] = 0;
			parseRS232CommandLine();
			commandLinePointer = 0;
		}
		break;

	default:
		if (newChar & 0x80) {
			return; //only accept ASCII
		}
		if (commandLinePointer < UART_COMMAND_LINE_MAX_LENGTH - 1) {
			if (enableUARTecho) {
				xputc(newChar);	  		   	// echo to indicate char arrived
			}
			commandLine[commandLinePointer++] = newChar;
		} else {
			commandLinePointer = 0;
			commandLine[commandLinePointer++] = newChar;
		}
	}  // end of switch

}  // end of rs232ParseNewChar

