function varargout = Head(varargin)
% Head MATLAB code for Head.fig
%      Head, by itself, creates a new Head or raises the existing
%      singleton*.
%
%      H = Head returns the handle to a new Head or the handle to
%      the existing singleton*.
%
%      Head('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Head.M with the given input arguments.
%
%      Head('Property','Value',...) creates a new Head or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Head_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Head_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Head

% Last Modified by GUIDE v2.5 08-Jul-2014 14:14:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Head_OpeningFcn, ...
                   'gui_OutputFcn',  @Head_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

    
% --- Executes just before Head is made visible.
function Head_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Head (see VARARGIN)

% Choose default command line output for Head
handles.output = hObject;

delete(instrfindall); %closes all currently open ports
handles.serial.s=serial('/dev/ttyUSB0','Baudrate',1000000,'Parity','none','DataBits',8,'Stopbits',1);
fopen(handles.serial.s);

%handles.joystick.joy=vrjoystick(1); %open joystick/gamepad

handles.timer.t=timer;    %timer to read the information from joystick and to set the changes
handles.timer.t.TimerFcn={@updateUI,handles};
handles.timer.t.StartDelay = 0;
handles.timer.t.Period=0.2;
handles.timer.t.ExecutionMode='fixedRate';
setAllPositions(handles);
start(handles.timer.t);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Head wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Head_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pbQuit.
function pbQuit_Callback(hObject, eventdata, handles)
stop(handles.timer.t);
delete(handles.timer.t);
delete(instrfindall);
close();
% hObject    handle to pbQuit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(1,round(get(handles.slider1,'Value')),handles);
start(handles.timer.t);

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
 
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1500);


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(2,round(get(handles.slider2,'Value')),handles);
start(handles.timer.t);

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1500);


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(3,round(get(handles.slider3,'Value')),handles);
start(handles.timer.t);


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1500);


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(4,round(get(handles.slider4,'Value')),handles);
start(handles.timer.t);


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1500); 


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(5,round(get(handles.slider5,'Value')),handles);
start(handles.timer.t);

% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1500);



% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
setPosition(6,round(get(handles.slider6,'Value')),handles);
start(handles.timer.t);

% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
set(hObject,'Value',1700);


% --- Executes on button press in pb_readPosition.
function pb_readPosition_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there can be an asynchronous write because of timer
position=readPosition(handles);
set(handles.edit_currentPositionID1,'String',position(1));
set(handles.edit_currentPositionID2,'String',position(2));
set(handles.edit_currentPositionID3,'String',position(3));
set(handles.edit_currentPositionID4,'String',position(4));
set(handles.edit_currentPositionID5,'String',position(5));
set(handles.edit_currentPositionID6,'String',position(6));
start(handles.timer.t);
% hObject    handle to pb_readPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pb_readVoltage.
function pb_readVoltage_Callback(hObject, eventdata, handles)
stop(handles.timer.t); pause(0.2);              %if not, there would be an asynchronous write because of timer
voltage=readVoltage(handles);
set(handles.edit_currentVoltageID,'String',(sprintf('%0.3f',voltage(1))));
start(handles.timer.t);
% hObject    handle to pb_readVoltage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit_id1_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id4_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_id5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_id6_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edit_id6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_id6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_currentPositionID1_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID2_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID3_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID4_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID5_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function edit_currentPositionID5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit_currentPositionID6_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function edit_currentPositionID6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentPositionID6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit_currentVoltageID_Callback(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_currentVoltageID as text
%        str2double(get(hObject,'String')) returns contents of edit_currentVoltageID as a double


% --- Executes during object creation, after setting all properties.
function edit_currentVoltageID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_currentVoltageID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
