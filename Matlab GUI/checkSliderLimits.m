function value=checkSliderLimits(value)
%without this function the slider would easily move outside their ranges
%and maybe the programm will stop running. So moving is made "foolproof"

if(value(1)>2495)
value(1)=2495; end
if(value(1)<486)
value(1)=486; end

if(value(2)>2466)
value(2)=2466; end
if(value(2)<1143)
value(2)=1143; end

if(value(3)>2466)
value(3)=2466; end
if(value(3)<1143)
value(3)=1143; end

if(value(4)>2466)
value(4)=2466; end
if(value(4)<893)
value(4)=893; end

if(value(5)>2073)
value(5)=2073; end
if(value(5)<475)
value(5)=475; end

if(value(6)>2063)
value(6)=2063; end
if(value(6)<1286)
value(6)=1286; end
