function []=setAllPositions(handles)
value(1)=round(get(handles.slider1,'Value'));
value(2)=round(get(handles.slider2,'Value'));
value(3)=round(get(handles.slider3,'Value'));
value(4)=round(get(handles.slider4,'Value'));
value(5)=round(get(handles.slider5,'Value'));
value(6)=round(get(handles.slider6,'Value'));

value=checkSliderLimits(value);

set(handles.slider1,'Value',value(1));
set(handles.slider2,'Value',value(2));
set(handles.slider3,'Value',value(3));
set(handles.slider4,'Value',value(4));
set(handles.slider5,'Value',value(5));
set(handles.slider6,'Value',value(6));
cmd = '!PA';
for j=1:6
    cmd = strcat(cmd, sprintf('%d,%d|', j, value(j)));
end
fwrite(handles.serial.s,sprintf('%s\n', cmd),'sync');
fgets(handles.serial.s); %echo
fgets(handles.serial.s); %reply: -PA