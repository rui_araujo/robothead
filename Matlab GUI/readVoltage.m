function [volt]=readVoltage(handles)
fwrite(handles.serial.s,sprintf('?V\n'),'sync');
fgets(handles.serial.s); %echo
reply = fgets(handles.serial.s);
volt=sscanf(reply, '-V %d');
volt(1) = volt(1)/1000;