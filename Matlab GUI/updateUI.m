function []=updateUI(myTimerObj, thisEvent, handles)

value(1)=round(get(handles.slider1,'Value'));
value(2)=round(get(handles.slider2,'Value'));
value(3)=round(get(handles.slider3,'Value'));
value(4)=round(get(handles.slider4,'Value'));
value(5)=round(get(handles.slider5,'Value'));
value(6)=round(get(handles.slider6,'Value'));

value=checkSliderLimits(value);

set(handles.slider1,'Value',value(1));
set(handles.slider2,'Value',value(2));
set(handles.slider3,'Value',value(3));
set(handles.slider4,'Value',value(4));
set(handles.slider5,'Value',value(5));
set(handles.slider6,'Value',value(6));
position=readPosition(handles);
set(handles.edit_currentPositionID1,'String',position(1));
set(handles.edit_currentPositionID2,'String',position(2));
set(handles.edit_currentPositionID3,'String',position(3));
set(handles.edit_currentPositionID4,'String',position(4));
set(handles.edit_currentPositionID5,'String',position(5));
set(handles.edit_currentPositionID6,'String',position(6));
voltage=readVoltage(handles);
set(handles.edit_currentVoltageID,'String',(sprintf('%0.3f',voltage(1))));
