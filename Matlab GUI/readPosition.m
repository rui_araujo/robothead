function [pos]=readPosition(handles,p)
fwrite(handles.serial.s,sprintf('?PA\n'),'sync');
fgets(handles.serial.s); %echo
reply = fgets(handles.serial.s);
pos = sscanf(reply, '-PA %d %d %d %d %d %d');